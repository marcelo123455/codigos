#include <stdio.h>
#include <math.h>
#include <stdlib.h>

void primeira();
void segunda();
void terceira();

int main (){
	int op;
	while(1){
		printf("\n(1) Diretamente mapeada com 16 blocos de uma palavra cada.\n");
		printf("(2) Diretamente mapeada com 4 blocos de 4 palavras cada.\n");
		printf("(3) Associativa por conjunto de 4 vias com 16 blocos ao todo, sendo cada bloco de uma palavra.\n");
		printf("(0) Sair.\n");
		printf("Opção: ");
		scanf("%d",&op);
		switch(op){
			case 1:
				primeira();
				break;
			case 2:
				segunda();
				break;
			case 3:
				terceira();
				break;
			case 0:
				return 0;
			default:
				printf("Opção inválida!.\n");
				break;
		}
	}
}

int resto(int val1, int val2){
	int res;
	res = val1 % val2;
	return res;
}

void primeira(){
	FILE *arq;
	int t1[16];
	int end, i, acerto, falha, val, ft=0, at=0;
	int NumConj = 16;
	for(i=0; i<16; i++){
		t1[i] = 0;
	}
	if((arq = fopen("enderecos.txt","r"))==NULL){
		printf("Erro ao abrir arquivo.\n");
		exit(1);
	}
	while((fscanf(arq,"%d",&end)) != EOF){
		acerto = 0;
		falha = 0;
		printf("\nEndereco: %d\n", end);
		val = resto(end, NumConj);
		printf("BLOCO: %d\n", val+1);
		if(t1[val] == end){
			printf("Acerto.\n");
			printf("Conjunto: %d\nVia: 1\n",val);
			acerto++;
		}
		if(t1[val] != end && t1[val] != 0){
			printf("Falha. (Outro dado no bloco)\n");
			falha++;
			t1[val] = end;
		}
		if(t1[val] == 0){
			printf("Falha. (Nenhum dado no bloco)\n");
			falha++;
			t1[val] = end;
		}
		//printf("\n");
		//for(i=0; i<16; i++){
			//printf("%d ",t1[i]);
		//}
		printf("\nFalhas: %d\n", falha);
		printf("Acertos: %d\n", acerto);
		ft = ft + falha;
		at = at + acerto;
	}
	printf("\nFalhas totais: %d\n",ft);
	printf("Acertos totais: %d\n",at);
}

void segunda(){
	FILE *arq;
	int t2[4];
	int end, i, acerto, falha, val, ft=0, at=0;
	int NumConj = 4;
	for(i=0; i<4; i++){
		t2[i] = 0;
	}
	if((arq = fopen("enderecos.txt","r"))==NULL){
		printf("Erro ao abrir arquivo.\n");
		exit(1);
	}
	while((fscanf(arq,"%d",&end)) != EOF){
		acerto = 0;
		falha = 0;
		printf("\nEndereco: %d\n", end);
		val = resto(end, NumConj);
		printf("BLOCO: %d\n", val+1);
		if(t2[val] == end){
			printf("Acerto.\n");
			printf("Conjunto: %d\nVia: 1\n",val);
			acerto++;
		}
		if(t2[val] != end && t2[val] != 0){
			printf("Falha. (Outro dado no bloco)\n");
			falha++;
			t2[val] = end;
		}
		if(t2[val] == 0){
			printf("Falha. (Nenhum dado no bloco)\n");
			falha++;
			t2[val] = end;
		}
		for(i=0; i<4; i++){
			printf("%d ",t2[i]);
		}
		printf("\nFalhas: %d\n", falha);
		printf("Acertos: %d\n", acerto);
		printf("\n");
		ft = ft + falha;
		at = at + acerto;
	}
	printf("Falhas totais: %d\n",ft);
	printf("Acertos totais: %d\n",at);
}

void terceira(){
	FILE *arq;
	int t3[4][4];
	int end, i, j, acerto, falha, val, ft=0, at=0, k=0, p=0;
	int NumConj = 4;
	for(i=0; i<4; i++){
		for(j=0; j<4; j++){
			t3[i][j] = 0;
		}
	}
	if((arq = fopen("enderecos.txt","r"))==NULL){
		printf("Erro ao abrir arquivo.\n");
		exit(1);
	}
	while((fscanf(arq,"%d",&end)) != EOF){
		p = 0;
		acerto = 0;
		falha = 0;
		printf("\nEndereco: %d\n", end);
		val = resto(end, NumConj);
		printf("BLOCO: %d\n", val);
		i=0;
		while(i < 4){
			if(t3[val][i] == end){
				printf("Acerto.\n");
				printf("Conjunto: %d\nVia: %d\n",val, i);
				acerto++;
				k = 1;
				p = 1;
			}
			if(k == 1)
				break;
			i++;
		}
		k=0;
		if(p == 0){
			for(i=0; i<4; i++){
				if(t3[val][i] == 0){
					printf("Falha. (Nenhum dado no bloco)\n");
					falha++;
					t3[val][i] = end;
					k = 1;
				}
				if(k==1)
					break;
				}
				k=0;
				for(i=i; i<4; i++){
					if(t3[val][i] != end && t3[val][i] != 0){
						printf("Falha. (Nenhum dado no bloco)\n");
						falha++;
						t3[val][i] = end;
						k = 1;
					}
					if(k==1)
						break;
				}
		}
		for(i=0; i<4; i++){
			for(j=0; j<4; j++){
				printf("%d ",t3[i][j]);
			}
			printf("\n");
		}
		printf("\nFalhas: %d\n", falha);
		printf("Acertos: %d\n", acerto);
		printf("\n");
		ft = ft + falha;
		at = at + acerto;
	}
	printf("Falhas totais: %d\n",ft);
	printf("Acertos totais: %d\n",at);
}
