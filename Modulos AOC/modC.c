//Arthur e Marcelo
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
	float T, freq, LatMem, LatCacheDados2, LatCacheInst2, CPIproce, TFCn1Dados, TFCn1Inst, TFCn2Dados, TFCn2Inst, PercLS;
	float CiclosDados2, CiclosRAM, CiclosInst2, CeDto2, CeDtoRAM, CeIto2, CeItoRAM, CPItotal2, CeDtoRAM1, CeItoRAM1, CPItotal1;
	printf("INFORME:\n");
	printf("\nFreqüência de operação do processador (em GHz): ");
	scanf("%f",&freq);
	printf("\nLatência para acesso a bloco na memória principal (em ns): ");
	scanf("%f",&LatMem);
	printf("\nLatência para acesso a cache de dados nível 2 (em ns): ");
	scanf("%f",&LatCacheDados2);
	printf("\nLatência para acesso a cache de instruções nível 2 (em ns): ");
	scanf("%f",&LatCacheInst2);
	printf("\nCPI básico do processador (quando executa sem falhas de cache): ");
	scanf("%f",&CPIproce);
	printf("\nTaxa de falhas na cache nível 1 de dados: ");
	scanf("%f",&TFCn1Dados);
	printf("\nTaxa de falhas na cache nível 1 de instruções: ");
	scanf("%f",&TFCn1Inst);
	printf("\nTaxa de falhas na cache nível 2 de dados: ");
	scanf("%f",&TFCn2Dados); 
	printf("\nTaxa de falhas na cache nível 2 de instruções: ");
	scanf("%f",&TFCn2Inst);
	printf("\nPercentual de instruções do tipo Load/Store: ");
	scanf("%f",&PercLS);
	
	T = 1/freq;
	CiclosDados2 = LatCacheDados2/T;
	CiclosRAM = LatMem/T;
	CiclosInst2 = LatCacheInst2/T; 
	CeDto2 = PercLS/100 * TFCn1Dados/100 * CiclosDados2;
	CeDtoRAM = PercLS/100 * TFCn1Dados/100 * TFCn2Dados/100 * CiclosRAM;
	CeIto2 = TFCn1Inst/100 * CiclosInst2;
	CeItoRAM = TFCn1Inst/100 * TFCn2Inst/100 * CiclosRAM;
	CPItotal2 = CeDto2 + CeDtoRAM + CeIto2 + CeItoRAM + CPIproce;
	
	CeDtoRAM1 = PercLS/100 * TFCn1Dados/100 * CiclosRAM;
	CeItoRAM1 = TFCn1Inst/100 * CiclosRAM;
	CPItotal1 = CeDtoRAM1 + CeItoRAM1 + CPIproce;
	
	printf("\nFreqüência de operação do processador (em GHz): %.2f", freq);
	printf("\nLatência para acesso a bloco na memória principal (em ns): %.2f",LatMem);
	printf("\nLatência para acesso a cache de dados nível 2 (em ns): %.2f",LatCacheDados2);
	printf("\nLatência para acesso a cache de instruções nível 2 (em ns): %.2f",LatCacheInst2);
	printf("\nCPI básico do processador (quando executa sem falhas de cache): %.2f",CPIproce);
	printf("\nTaxa de falhas na cache nível 1 de dados: %.2f",TFCn1Dados);
	printf("\nTaxa de falhas na cache nível 1 de instruções: %.2f",TFCn1Inst);
	printf("\nTaxa de falhas na cache nível 2 de dados: %.2f",TFCn2Dados);
	printf("\nTaxa de falhas na cache nível 2 de instruções: %.2f",TFCn2Inst);
	printf("\nPercentual de instruções do tipo Load/Store: %.2f",PercLS);
	printf("\nCPI final com falhas (Com Cache nivel 2): %.2f", CPItotal2);
	printf("\nCPI final com falhas (Sem Cache nivel 2): %.2f", CPItotal1);
	return 0;
}
