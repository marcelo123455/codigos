#include <stdio.h>
#include <math.h>

int main(){
	int CapCacheKB, NumVias, LargBytesPalavra, NumConjuntos, TamBlocoMemoria, TamTag, Indice, OffB, OffP;
	float Overhead, TamTotal;
	printf("Informe:\n");
	printf("Capacidade (em KB) da cache para armazenamento de informações do usuário(dados ou instruções): ");
	scanf("%d",&CapCacheKB);
	printf("Largura (em bytes) da palavra (tanto para dados como endereços): ");
	scanf("%d",&LargBytesPalavra);
	printf("O tamanho do bloco de memória (em palavras): ");
	scanf("%d",&TamBlocoMemoria);
	printf("Numero de vias da cache: ");
	scanf("%d", &NumVias);
	NumConjuntos = (CapCacheKB*8*1024 / (TamBlocoMemoria * LargBytesPalavra*8));
	NumConjuntos = NumConjuntos/NumVias;
	Indice = log2(NumConjuntos);
	OffB = log2(LargBytesPalavra);
	OffP = log2(TamBlocoMemoria);
	TamTag = (LargBytesPalavra * 8) - Indice - OffB - OffP; 
	Overhead = NumConjuntos * (TamTag + 3) * NumVias;
	Overhead = (Overhead/(8*1024));
	TamTotal = Overhead + CapCacheKB;
	printf("\nCapacidade (em KB) da cache para armazenamento de informações do usuário(dados ou instruções): %d",CapCacheKB);
	printf("\nLargura (em bytes) da palavra (tanto para dados como endereços): %d",LargBytesPalavra);
	printf("\nO tamanho do bloco de memória (em palavras): %d",TamBlocoMemoria);
	printf("\nIndice: %d",Indice);
	printf("\nOffset de Byte: %d",OffB);
	printf("\nOffset de Palavra: %d",OffP);
	printf("\nTamanho da Tag: %d",TamTag);
	printf("\nOverhead: %.2f",Overhead);
	printf("\nNumero de conjuntos da cache: %d",NumConjuntos);
	printf("\nTamanho total: %.2f \n",TamTotal);
	return 0;
}
