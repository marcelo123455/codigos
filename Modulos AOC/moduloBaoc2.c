#include <stdio.h>
#include <math.h>

int main(){
	int e = 0, Over, CapCacheKB, LargBytesPalavra, TamBlocoMemoria, NumConjuntos, TamTag, Indice, OffB, OffP, IndBits, LargEnd, OffTotal, TamBlocoBytes;
	float Overhead, TamTotal, TamTag2;
	
	while(e != 3){
		printf("Informe o que deseja fazer: \n");
		printf("1 - Calcular em KB o tamanho total da cache \n");
		printf("2 - Calcular o tamanho do bloco de memoria \n");
		printf("3 - Sair \n");
		scanf("%i",&e);

		switch (e){
			case 1:
				printf("Informe:\n");
				printf("Capacidade (em KB) da cache para armazenamento de informações do usuário(dados ou instruções): ");
				scanf("%d",&CapCacheKB);
				printf("Largura (em bytes) da palavra (tanto para dados como endereços): ");
				scanf("%d",&LargBytesPalavra);
				printf("O tamanho do bloco de memória (em palavras): ");
				scanf("%d",&TamBlocoMemoria);
				/* =============
					CALCULOS
				============== */
				NumConjuntos = (CapCacheKB * 1024)/((TamBlocoMemoria)*(LargBytesPalavra));
				Indice = log2(NumConjuntos);
				OffB = log2(LargBytesPalavra);
				OffP = log2(TamBlocoMemoria);
				TamTag = (LargBytesPalavra * 8) - Indice - OffB - OffP; 
				Overhead = NumConjuntos * (TamTag + 1);
				Overhead = (Overhead/(8*1024));
				TamTotal = Overhead + CapCacheKB;
	
				/* =======================
					IMPRESSÕES PARTE 1
				====================== */
				printf("\nCapacidade (em KB) da cache para armazenamento de informações do usuário(dados ou instruções): %d",CapCacheKB);
				printf("\nLargura (em bytes) da palavra (tanto para dados como endereços): %d",LargBytesPalavra);
				printf("\nO tamanho do bloco de memória (em palavras): %d",TamBlocoMemoria);
				printf("\nNumero de conjuntos da cache: %d",NumConjuntos);
				printf("\nIndice: %d",Indice);
				printf("\nOffset de Byte: %d",OffB);
				printf("\nOffset de Palavra: %d",OffP);
				printf("\nTamanho da Tag: %d",TamTag);
				printf("\nOverhead: %.2f",Overhead);
				printf("\nTamanho total: %.2f \n",TamTotal);
			break;
			case 2:
				printf("Informe o numero de bits do campo Indice: ");
				scanf("%i",&IndBits);
				printf("Informe a largura (em bytes) do endereço: ");
				scanf("%i",&LargEnd);
				printf("Informe o Overhead (em bytes) da cache: ");
				scanf("%i",&Over);

				printf("Indice em bits: %i \n", IndBits);
				printf("Largura da palavra em bytes: %i \n", LargEnd);
				printf("Overhead em Bytes: %i \n", Over);
				
				NumConjuntos = pow(2,IndBits);
				printf("Numero de conjuntos: %i \n", NumConjuntos);
				TamTag2 = (((Over*8) - NumConjuntos) / NumConjuntos); 
				TamTag = round(TamTag2);
				printf("Tamanho da tag: %i \n", TamTag);
				OffTotal = ((LargEnd*8) - (TamTag + IndBits));
				printf("tamanho do offset total: %i \n", OffTotal);
				TamBlocoBytes = pow(2,OffTotal);

				printf("Tamanho do bloco em Bytes: %i \n", TamBlocoBytes);
				
			break;
			default:
			 if(e != 3){
			 	printf("Opcao Invalida Informe novamente: ");
			 	scanf("%i",&e);
			 }
			break;
		}
	}
	return 0;
}
