#include <stdio.h>
#include <math.h>

	int main(){
	float CapacidadeKB, LarguraByte, CapacidadeByte, OffsetByte, Conjuntos, Indice, Tag, TamTotal;
	printf("informe:"\n);	
	printf("Capacidade (em KB) da cache: ");
	scanf("%f",&CapacidadeKB);
	printf("Capacidade (em bytes) da palavra: ");
	scanf("%f",&LarguraByte)
	OffsetByte = log2(LarguraByte);
	CapacidadeByte = CapacidadeKB * 1024;
	Conjuntos = (CapacidadeByte / LarguraByte);
	Indice = log2(Conjuntos);
	Tag = (8 * LarguraByte) - Indice - OffsetByte;
	TamTotal =(((Tag + 1) * Conjuntos) + CapacidadeKB)/8;
	TamTotal = (TamTotal/1024);
	printf("\nCapacidade (em KB) da cache: %.2f", CapacidadeKB);
	printf("\nCapacidade Total (em KB) da cache: %.2f", TamTotal);
	printf("\nLargura (em bytes) da palavra: %.2f", LarguraByte);
	printf("\nOffset de byte do endereço: %.2f", OffsetByte);
	printf("\nConjuntos da cache: %.2f",Conjuntos);
	printf("\nTamanho do indice: %.2f", Indice);
	printf("\nTamanho do campo tag: %.2f \n", Tag);
	return 0;
}
