// Pesquisa e Classificação de Dados - Atividade Semi-Presencial 04
// 20/04/2018
// Mateus Soares (mateus123soares@hotmail.com) , Marcelo Marchioro Cordeiro 2 (marcelo_1411@hotmail.com).

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

struct bd{
	int matricula;
	char nome[40];
	int faltas;
	float nota;
};
typedef struct bd Bd; 

void inserir(Bd *bd,int x);
void consultar(Bd *bd,int x);
int excluir(Bd *bd,int x);
void exibir(Bd *bd,int x);
void encerrar(Bd *bd,int x);
int BuscaBinaria(Bd *bd,int chave,int x);
void BuscaSequencial(Bd *bd, int chave,int qtde);
void inserirManual(Bd *bd,int x);

int main(){
int e,tam,x=0;
printf("Informe o tamanho do vetor \n");
scanf("%d",&tam);
Bd *bd=(Bd*)malloc(sizeof(Bd)*tam);

	while(e!=6){
		printf("1 - Inserir alunos automaticamente\n");
		printf("2 - Inserir alunos manualmente\n");
		printf("3 - consultar aluno\n");
		printf("4 - excluir aluno\n");
		printf("5 - exibir registros\n");
		printf("6 - encerrar\n");
		scanf("%d",&e);
			if(e==1){
				x=tam;		
				inserir(bd,x);			
			}
			if(e==2){		
				inserirManual(bd,x++);
			}
			if(e==3){		
				consultar(bd,x);
			}
			if(e==4){		
				x = excluir(bd,x);
			}
			if(e==5){		
				exibir(bd,x);
			}
			if(e==6){		
				free(bd);
			}
	}
return 0;
}

void inserir(Bd *bd,int x){
	int str_len,k;
	char *novastr;
	char *validchars = "abcdefghijklmnopqrstuvwxyz ";
	srand(time(NULL));
	printf("Feito. \n",x);
	str_len=rand() % 20;
	novastr = ( char * ) malloc ( (str_len + 1) * sizeof(char));
     for (k=0; k!=x; k++){
     	for ( int i = 0; i < str_len; i++ ) {
			novastr[i] = validchars[ rand() % strlen(validchars) ];
			novastr[i+1] = 0x0;
		}
		strcpy(bd[k].nome,novastr);
     	bd[k].matricula=k + 1;
		bd[k].faltas=rand() % 60;
		bd[k].nota=rand() % 10;
	}
}
void inserirManual(Bd *bd,int x){
	int matricula, c;
		printf("Digite a matricula \n");
		scanf("%d",&matricula);
		for(int i=0;i!=x;i++){
			while(bd[i].matricula==matricula){
				printf("matricula já registrada\n");
				scanf("%d",&matricula);
			}
		}
		bd[x].matricula=matricula;
		printf("Digite o nome do aluno \n");
		setbuf(stdin,NULL);
		scanf("%[^\n]",bd[x].nome);
		setbuf(stdin,NULL);
		printf("Digite o numero de faltas \n");
		scanf("%i",&bd[x].faltas);
		printf("Digite a nota \n");
		scanf("%f",&bd[x].nota);
}


int excluir(Bd *bd,int x){
int flag = 0;
int matricula, j;

	printf("Informe a matricula a ser excluida \n");
	scanf("%d",&matricula);
	if(matricula == bd[x-1].matricula){
		x--;
		flag=1;
	}
	else{
		for(int i=0;i!=x;i++){
			if(bd[i].matricula==matricula){
				flag=1;
				j = i;
				x--;
			}
			if(flag=1){
				for(;j<x;j++){
					bd[j]=bd[j+1];
				}	
			}
		}
	}
	if(flag==0){
		printf("matricula não encontrada\n");	
	}	
	return x;
}
void exibir(Bd *bd,int x){
	for(int i=0;i!=x;i++){
		printf("Matricula: %d  ",bd[i].matricula);
		printf("Nome: %s  ",bd[i].nome);
		printf("Faltas: %i  ",bd[i].faltas);
		printf("Notas: %f  ",bd[i].nota);
		printf("\n");		
	}
} 

void consultar(Bd *bd,int x){
	int r, i, k;
	while(r != 3){
		printf("Informe o tipo de busca voce deseja fazer: \n");
		printf("1 - Pesquisa sequencial \n");
		printf("2 - Pesquisa binaria \n");
		printf("3 - Voltar \n");
		scanf("%i",&r);
		switch(r){
			case 1:
				BuscaSequencial(bd,k,x);
        		
			break;
			
			case 2:
			    printf("Digite a matricula :");
        		fflush(stdin);
        		scanf("%d", &k);
        		fflush(stdin);
        		i = BuscaBinaria(bd,k,x);
        		if (i < 0)
          			printf("Registro nao encontrado\n");
        		else {
          			printf("Registro encontrado: \n");
            		printf("matricula: %i nome: %s faltas: %i nota: %.2f \n", bd[i].matricula,
            		bd[i].nome, bd[i].faltas, bd[i].nota);
        		}
			break;

			default:
				if(r != 3){
					printf("Opcao Invalida");
				}
				else{
					return;
				}
			break;
		}	
	}
}

	int BuscaBinaria(Bd *bd,	int chave,int x){
	int ini, fim, meio, count = 0;

 	ini = 0;
 	fim = x - 1;
	while (ini <= fim) {
    	count++;
    	meio = (ini + fim) / 2;
    	if(chave < bd[meio].matricula)
    	  	fim = meio - 1;
    	else if (chave > bd[meio].matricula)
      		ini = meio + 1	;
    	else {
      		printf("Quantidade de registros visitados: %ld\n", count);
      		return meio;
    	}
  	}
  	printf("Quantidade de registros visitados: %ld\n", count);
	return -1;
}

void BuscaSequencial(Bd *bd, int chave, int qtde){
	int k,i,count;
	
	printf("Digite a matricula :");
    fflush(stdin);
    scanf("%d", &k);
    fflush(stdin);
    count = 0;
    for (i = 0; i < qtde; i++) {
        count++;
        if (bd[i].matricula == k) {
            printf("Registro encontrado: \n");
            printf("matricula: %i nome: %s faltas: %i nota: %.2f \n", bd[i].matricula,
            bd[i].nome, bd[i].faltas, bd[i].nota);
         	break;
        }
    }
    if (count >= qtde)
        printf("REGISTRO NAO ENCONTRADO \n");
        printf("Quantidade de registros visitados: %d\n", count);
}






