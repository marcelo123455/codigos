
struct filme{
	int cod;
	char titulo[100];
	int ano;
	int quantidade;
	char genero[10];

};
typedef struct filme Filme;

Filme *inicio();
FILE *abrearquivo(char modo,FILE *arquivo);
Filme *vetor(FILE *arquivo);
void imprimeArcervo(FILE *arquivo, Filme *vet);
void relatorio(FILE *arquivo, Filme *vet);
Filme *locacao (FILE *arquivo, Filme *vet);
Filme *devolucao (FILE *arquivo, FILE *arquivo2, Filme *vet);
void busca(FILE *arquivo, Filme *vet);
