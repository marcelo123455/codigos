#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "func.h"

//fu��o para escolher o modo de abertura do arquivo
FILE *abrearquivo(char modo,FILE *arquivo){
	switch(modo){
		case 'g':		
			if((arquivo=fopen("entrada.txt","a"))==NULL){
				printf("erro na abertura \n");
		}
		break;
		case 'r':		
			if((arquivo=fopen("entrada.txt","rt"))==NULL){
				printf("erro na abertura \n");
			}
		break;
	}
	
	if(arquivo==NULL){
			printf("Permiss�o negada \n");
	}
	return arquivo;
}

//Fun��o que salva cada filme em um vetor struct
Filme *vetor(FILE *arquivo){
	char cont[256],num[256];
	char *palv;
	int t, j, x = 0;
	
	arquivo = abrearquivo('r',arquivo);
	fgets(cont,20,arquivo);
	t = atoi(cont);
	Filme *vet = (Filme *)malloc(t * sizeof(Filme));
	while (x != t){ // faz a separacao das palavras a cada ; ou \n
		fgets(cont,200,arquivo);
		palv = strtok(cont,";");
		strcpy(vet[x].titulo,palv);
		palv = strtok(NULL,";");
		strcpy(num,palv);
		j = atoi(num);
		vet[x].ano = j;
		palv = strtok(NULL,";");
		strcpy(num,palv);
		j = atoi(num);
		vet[x].quantidade = j; 
		palv = strtok(NULL,"\n");
		strcpy(vet[x].genero,palv);
		vet[x].cod = x+1;		
		x++;		
	}
	fclose(arquivo);
return vet;
}

//fun��o para imprimir todo o acervo
void imprimeArcervo(FILE *arquivo, Filme *vet){
	int t, x = 0;
	char cont[256];
	arquivo = abrearquivo('r',arquivo);
	fgets(cont,20,arquivo);
	t = atoi(cont);
	while(x != t){
		printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n\n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
		x++;
	}
	fclose(arquivo);
}

//exibe informa��o de filmes por ano ou genero caso tenha no acervo
void relatorio(FILE *arquivo, Filme *vet){
	int d = 0, t, x = 0, a = 0, ano1, p = 0;
	char cont[256], gen[100];
	FILE *arquivo5;
	FILE *arquivo4;
	arquivo = abrearquivo('r',arquivo);
	fgets(cont,20,arquivo);
	t = atoi(cont);
	while(d != 4){
		printf("Informe o criterio de busca para o relatorio. \n");
		printf("1 - GENERO. \n");
		printf("2 - ANO. \n");
		printf("3 - ACERVO COMPLETO \n");
		printf("4 - VOLTAR. \n");
		scanf("%i",&d);
		switch (d){
			case 1: // por genero
				x = 0;
				a = 0;
				remove("relatorio.txt");
				printf("Informe o genero a ser pesquisado. \n ");
				scanf("%s",gen);
				while(x != t){
					if(strcasecmp(gen,vet[x].genero) == 0){
						if((arquivo5 = fopen("relatorio.txt","a"))!=NULL){
							fprintf(arquivo5,"%s;%i;%i;%s\n",vet[x].titulo,vet[x].ano,vet[x].quantidade,vet[x].genero);
							fclose(arquivo5);
							printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n\n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
							a++;
						}
					}
					x++;				
				}
				if(a == 0){
					printf(" Nao existe filme desse genero no acervo!!! \n");
				}
			break;
			case 2: // por ano
				x = 0;
				a = 0;
				remove("relatorio.txt");
				printf("Informe o ano a ser pesquisado. \n ");
				scanf("%i",&ano1);
				while(x != t){
					if(ano1 == vet[x].ano){
						if((arquivo5 = fopen("relatorio.txt","a"))!=NULL){
							fprintf(arquivo5,"%s;%i;%i;%s\n",vet[x].titulo,vet[x].ano,vet[x].quantidade,vet[x].genero);
							fclose(arquivo5);
							printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n\n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
							a++;
						}
					}
					x++;				
				}
				if(a == 0){
					printf(" Nao existe filme desse ano no aceervo!!! \n");
				}	
			break;
			case 3:// acervo todo
				x = 0;
				p = 0;
				remove("acervo_completo_atual.txt");
				while(x != t){
					if((arquivo4 = fopen("acervo_completo_atual.txt","a"))!=NULL){
						if(p == 0){
							fprintf(arquivo4,"%i\n",t);
							p++;
						}
						fprintf(arquivo4,"%s;%i;%i;%s\n",vet[x].titulo,vet[x].ano,vet[x].quantidade,vet[x].genero);
						fclose(arquivo4);					
					}
					x++;
				}
	 			x = 0;
				while(x != t){
					printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n\n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
					x++;
				}
			break;

			default:
				if(d != 4){
					printf("Opcao invalida. Por favor escolha novamente. \n");
				}
				else{
					fclose(arquivo);
				}
			break;
		}
	}
}

//fun��o para fazer loca��o de filme
Filme *locacao (FILE *arquivo, Filme *vet){
	int t, x = 0, a = 0, y = 0, p = 0, code = 0;
	char cont[256];
	arquivo = abrearquivo('r',arquivo);
	fgets(cont,20,arquivo);
	t = atoi(cont); // pega o numero de filmes do acervo
	fclose(arquivo);
	printf("Informe o codigo do filme a ser locado: \n");
	scanf("%i",&code);
	while (x != t){
		if(code == vet[x].cod && vet[x].quantidade != 0){ //verifica se � possivel fazer a loca�ao
			FILE *arquivo3;
			vet[x].quantidade = vet[x].quantidade - 1;
			printf("Alugado com sucesso \n");
			while(y != t){ // atuzliza o banco de dados
				if((arquivo3 = fopen("copia.txt","a"))!=NULL){
					if(p == 0){
						fprintf(arquivo3,"%i\n",t);
						p++;
					}
						fprintf(arquivo3,"%s;%i;%i;%s\n",vet[y].titulo,vet[y].ano,vet[y].quantidade,vet[y].genero);
						fclose(arquivo3);					
				}
				y++;
			}
			a++;
			remove("entrada.txt");
			rename("copia.txt","entrada.txt");		
		}
		x++;
	}
	if(a == 0){
		printf("Impossivel realizar locacao!! \nQuantidade insuficiente ou filme inexistente no acervo\n");
	}
	return vet;
}

//faz a devolu��o de um filme 
Filme *devolucao (FILE *arquivo, FILE *arquivo2, Filme *vet){
	int t, x = 0, a = 0, c = 0, y = 0, p = 0, code = 0;
	char cont[256],cont2[256],num[256];
	char *palv;
	arquivo2 = fopen("Bdados.txt","r");
	arquivo = abrearquivo('r',arquivo);
	fgets(cont,20,arquivo);
	t = atoi(cont); // pega o numero de filmes do acervo
	fclose(arquivo);
	printf("Informe o codigo do filme a ser devolvido: \n");
	scanf("%i",&code);
	while (x != t){ //pega a quantidade maxima dos filmes
		fgets(cont2,200,arquivo2);
		palv = strtok(cont2,";");
		strcpy(num,palv);
		c = atoi(num);
	
		if(code == vet[x].cod && vet[x].quantidade < c){ //verifica se � possivel fazer a devolu��o
			FILE *arquivo3;
			vet[x].quantidade = vet[x].quantidade + 1;
			printf("Filme devolvido com sucesso \n");
			while(y != t){ // atuzliza o banco de dados
				if((arquivo3 = fopen("copia.txt","a"))!=NULL){
					if(p == 0){
						fprintf(arquivo3,"%i\n",t);
						p++;
					}
						fprintf(arquivo3,"%s;%i;%i;%s\n",vet[y].titulo,vet[y].ano,vet[y].quantidade,vet[y].genero);
						fclose(arquivo3);					
				}
				y++;
			}
			a++;
			remove("entrada.txt");
			rename("copia.txt","entrada.txt");			
		}
	x++;
	}
	if(a == 0){
		printf("Impossivel realizar Devolucao!! \nQuantidade maxima atingida ou filme inexistente no acervo\n");
	}
	fclose(arquivo2);
	return vet;
}

// busca um determinado filme do acervo
void busca(FILE *arquivo, Filme *vet){
	char cont[256], nome[100], gen[100];
	int t = 0, codi = 0, e = 0, x = 0, a = 0, ano1 = 0;
	
	arquivo = abrearquivo('r',arquivo);
	fgets(cont,20,arquivo);
	t = atoi(cont); // pega o numero de filmes do acervo
	fclose(arquivo);
	
	while (e != 5){
		a = 0;
		x = 0;
		printf("Informe o criterio de busca \n");
		printf("1 - Titulo \n");
		printf("2 - Codigo \n");
		printf("3 - Genero \n");
		printf("4 - Ano \n");
		printf("5 - Voltar \n");
		
		scanf("%i",&e);
		switch(e){
			case 1://por titulo
				printf("Informe o titulo a ser buscado:\n");
				setbuf(stdin,NULL);
				scanf("%[^\n]",nome);
				while(x != t){
					if((strcasecmp(nome,vet[x].titulo) == 0)){ //verifica se tem o filme no acervo
						printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n\n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
						a++;
					}
					x++;
				}
				if(a == 0){
					printf("Impossivel realizar locacao!! \nFilme inexistente no acervo\n");
				}		
			break;
			case 2: // por codigo
				printf("Informe o codigo a ser buscado:\n");
				scanf("%i",&codi);
				while(x != t){
					if((vet[x].cod == codi)){ //verifica se tem o filme no acervo
						printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n\n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
						a++;
					}
					x++;
				}
				if(a == 0){
					printf("Impossivel realizar locacao!! \nFilme inexistente no acervo\n");
				}		
				
			break;
			case 3: // por genero
				printf("Informe o genero a ser pesquisado:\n");
				scanf("%s",gen);
				while(x != t){
					printf("%d %s  %d %s \n ",strlen(gen),gen,strlen(vet[x].genero),vet[x].genero);
					if(strcasecmp(gen,vet[x].genero) == 0){
						printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
						a++;
				}
					x++;
				}
				if(a == 0){
					printf("Nenhum filme encontrado para esse genero \n");
				}
			break;
			case 4: // por ano
				printf("Informe o ano a ser pesquisado:\n");
				scanf("%i",&ano1);
				while(x != t){
					if(ano1 == vet[x].ano){
						printf("\nTitulo: %s \nGenero: %s \nAno: %i \nCodigo: %i \nQuantidade: %i \n",vet[x].titulo,vet[x].genero,vet[x].ano,vet[x].cod,vet[x].quantidade);
						a++;
				}
					x++;
				}
				if(a == 0){
					printf("Nenhum filme encontrado para esse ano \n");
				}
			break;
			default:
				if (e != 5){
					printf("Opcao invalida!!! \n");
				}			
			break;
		}
	}
}
