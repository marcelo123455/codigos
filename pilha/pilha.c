#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"

Pilha *CriaPilha(){
	Pilha *pl = (Pilha*)malloc(sizeof(Pilha));
	pl->quantidade = 0;
	pl->topo = NULL;
	return pl;
} 

Pilha *pilhaPush(Pilha *pl){
	char n[50];
	float med;
	int mat;
	Aluno *aux = pl->topo;
	Aluno *a =(Aluno*)malloc(sizeof(Aluno));
	printf("Informe o nome do aluno: ");
	setbuf(stdin,NULL);
	scanf("%[^\n]",n);
	printf("Informe a matricula do aluno: ");
	scanf("%i",&mat);
	while(aux != NULL){
		if(aux->matricula == mat){
			while(aux->matricula == mat){
			printf("Matricula já existente, por favor digite um outro numero de matricula para o aluno: ");
			scanf("%i",&mat);
			}
		}
		aux = aux->prox;
	}

	printf("Informe a media do aluno: ");
	scanf("%f",&med);
	while(med < 0 || med > 10){
		printf("Valor para media invalido, digite novamente: ");
		scanf("%f",&med);
	}
	strcpy(a->nome,n);
	a->matricula = mat;
	a->media = med;
	a->prox = pl->topo;
	pl->topo = a;
	pl->quantidade++;
	return pl;
}

void todaPilha(Aluno *topo){
	
	Aluno *aux = topo;

	while(aux != NULL){
		printf("Nome: %s\nMatricula: %i\nMedia: %.2f\n\n",aux->nome,aux->matricula,aux->media);
		aux = aux->prox;
	}
	free(aux);
}

Pilha *pop(Pilha *pl){
	if(pl->topo == NULL){
		printf("Cadastro Vazio!");
		return NULL;
	}
	else{
		Aluno *aux = pl->topo;
		pl->topo = pl->topo->prox;
		pl->quantidade--;
		printf("O aluno %s de matricula: %i foi excluido do cadastro \n",aux->nome,aux->matricula);
		free(aux);
		return pl;
	}
}

void busca(Aluno *topo){
	int i = 0, mat = 0;
	char n[50];
	Aluno* aux = topo;

	printf("Informe como deseja fazer a busca \n");
	printf("1 - Por Nome \n");
	printf("2 - Por Matricula\n");
	printf("3 - Voltar \n");
	scanf("%i",&i);
	switch(i){
			int z = 0;
		case 1:
			z = 0;
			printf("Informe o nome do aluno: ");
			setbuf(stdin,NULL);
			scanf("%[^\n]",n);
		 	while(aux != NULL){
				if(strcasecmp(aux->nome,n) == 0){
					printf("Nome: %s\nMatricula: %i\nMedia: %.2f\n\n",aux->nome,aux->matricula,aux->media);
					z++;
				}
				aux = aux->prox;
			}
			if(z == 0){
				printf("Nenhum aluno com esse nome foi encontrado no cadastro \n");
			}
		break;
		case 2:
			z = 0;
			printf("Informe a matricula do aluno: ");
			scanf("%i",&mat);
		 	while(aux != NULL){
				if(aux->matricula == mat){
					printf("Nome: %s\nMatricula: %i\nMedia: %.2f\n\n",aux->nome,aux->matricula,aux->media);
					z++;
				}
				aux = aux->prox;
			}
			if(z == 0){
				printf("Nenhum aluno com essa matricula foi encontrado no cadastro \n");
			}
		break;
		default:
			if(i != 3){
				printf("Opcao Invalida! \n");
			}
			else{
				return;
			}
		break;
	}
}

void libera(Pilha *pl){
	Aluno* aux;

	if(pl->topo == NULL){
		return;
	}
	else{
		while(aux != NULL){
			aux = pl->topo->prox;
			free(pl->topo);
			pl->topo = aux;
		}
	}
}