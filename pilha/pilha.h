#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct alunos{

	int matricula;
	char nome[50];
	float media;
	struct alunos *prox;

};
typedef struct alunos Aluno;

struct descritorPilha{

	int quantidade;
	Aluno *topo;
};
typedef struct descritorPilha Pilha;

Pilha *CriaPilha();
Pilha *pilhaPush(Pilha *pl);
void todaPilha(Aluno *topo);
Pilha *pop(Pilha *pl);
void busca(Aluno * topo);
void libera(Pilha *pl);