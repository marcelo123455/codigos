//Biblioteca Heap

//função que constroi um novo heap com o elemento inserido
int *buildHeap (int *heap,int tamanho,int elemento);

//função para deletar o elemento da heap
int deleteHeap(int *heap,int tamanho);

//função para imprimir o estado atual da heap
void printHeap(int *heap,int tamanho);

//função sift-down para posicionar os elementos da heap
void siftDown(int *heap,int tamanho);

//função percolate para posicionar os elementos da heap
void percolate(int*heap,int tamanho);

//retorna o menor da heap 
int getRaiz(int *heap);