#include <stdio.h>
#include <stdlib.h>
#include "Heap.h"

int *buildHeap(int *heap,int tamanho,int elemento){
	int *vetor, x, j;
	vetor = (int*)malloc(sizeof(int) * tamanho);
	
	if(heap != NULL){
		for(x = 0; x < tamanho; x++){
			vetor = heap;
		}
	}
	vetor[tamanho - 1] = elemento;
	heap = vetor;
	
	percolate(heap,tamanho);
	return heap;
}

void percolate(int *heap,int tamanho){
	
	int aux, i;
	i = tamanho - 1;
	for(int x = 0; x < tamanho; x++){
		if(heap[i] < heap[i - 1]){
			aux = heap[i -1];
			heap[i - 1] = heap[i];
			heap[i] = aux;
		}
		i--;
	}
}

void printHeap(int *heap,int tamanho){
	int x;

	for(x = 0; x < tamanho; x++){
		printf("[%i]",heap[x]);
	} 
}

int deleteHeap(int *heap,int tamanho){
	int aux, *vetaux;
	aux = heap[0];
	heap[0] = heap[tamanho - 1];
	vetaux = heap;
	heap=(int*)malloc(sizeof(int) * (tamanho - 1));
	heap = vetaux;
	siftDown(heap,tamanho - 1);
	return aux;
}

void siftDown(int *heap,int tamanho){

	int aux;
	for(int x = 0; x < tamanho; x++){
		if(heap[x] > heap[x + 1]){
			aux = heap[x + 1];
			heap[x + 1] = heap[x];
			heap[x] = aux;
		}
	} 
}

int getRaiz(int *heap){
	int aux;
	aux = heap[0];
	printf("A raiz é o valor: [%i] \n",aux);
	return aux;
}