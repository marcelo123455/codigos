#include <stdio.h>
#include <stdlib.h>
#include "func.h"

int *makeSet(int n){
	int *conj = (int*)malloc(n * sizeof(int));
	for(int i = 1; i <= n; i++){
			conj[i] = i;
	}
	return conj;
}

int findSet(int i, int conj[]){
	if(conj[i] == i){
		return i;
	}
	else{
		conj[i] = findSet(conj[i],conj);
		return conj[i];
	}
}

int *unir(int i, int j, int conj[]){
	conj[i] = findSet(j,conj);

	return conj;
}