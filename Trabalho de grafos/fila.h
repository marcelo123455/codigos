

struct lista {
	int info;
	struct lista *prox;	
};
typedef struct lista Lista;

struct filalista{
	Lista *ini;
	Lista *fim;
};
typedef struct filalista filaLista;

filaLista *criarFilaLista();
void insereFilaLista(filaLista *fl, int a);
int retiraFilaLista(filaLista *fl);
void liberaFilaLista(filaLista *fl);
