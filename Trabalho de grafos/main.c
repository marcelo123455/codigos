//Marcelo Marchioro Cordeiro (marcelo_1411@hotmail.com)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grafos.h"
#include "pilha.h"
#include "fila.h"
#include "conjunto.h"
#include "heap.h"

int main(){
	int i = 0, e = 1, n = 0, valor = 0, part = 0, dest = 0, peso = 0, chave = 0, v = 0, *resultado, *vetor, a, b, tamanho = 0, w = 0;
	Aresta *heap = NULL, *removido, *raiz;
	char aux[50], num[50], *palv;
	Grafo *g = Inicia();
	FILE *arquivo;
	Nodo *no = g->ListaNodos;
	Aresta *aux1;

	while(e != 0){
		printf("Informe o que deseja fazer: \n");
		printf("1 - Gerar o grafo \n");
		printf("2 - Fazer DFS \n");
		printf("3 - Fazer BFS \n");
		printf("4 - Conjunto Disjunto \n");
		printf("5 - Heap \n");
		printf("6 - Kruskal\n");
		printf("7 - Prim\n");
		printf("8 - Dijkstra\n");
		printf("9 - Mostrar o grafo\n");
		printf("0 - Sair \n");
		scanf("%i",&e);
		switch(e){
			e = 0;
			case 1:
				arquivo = abrearquivo('r',arquivo);
				fgets(aux,20,arquivo);
				n = atoi(aux);
				for(i = 0; i < n; i++){
					valor = valor + 1;
					g->ListaNodos = criaNodo(g->ListaNodos,valor);
					g->tamanho++;
				}
				while ((fgets(aux,49,arquivo))!= NULL){ // faz a separacao das palavras a cada ; ou \n
					palv = strtok(aux,";");
					strcpy(num,palv);
					part = atoi(num);
					palv = strtok(NULL,";");
					strcpy(num,palv);
					dest = atoi(num); 
					palv = strtok(NULL,"\n");
					strcpy(num,palv);
					peso = atoi(num);
					g->ListaNodos = adj(g->ListaNodos,part,dest,peso);		
				}

				fclose(arquivo);
				
				no = g->ListaNodos;
				
				while(no != NULL){
					aux1 = no->adj;
					while(aux1 != NULL){
						printf("Chave: %i, ADJ: %i, Peso: %i \n",no->chave, aux1->chave_adj,aux1->peso);
						aux1 = aux1->prox;
					}
					no=no->prox;
    			}	
			break;

			case 2:
				printf("Informe por onde deseja comecar a buscar: ");
				scanf("%i",&chave);
				dfs(g,chave);
			break;

			case 3:
				printf("Informe por onde deseja comecar a buscar: ");
				scanf("%i",&chave);
				bfs(g,chave);
			break;

			case 4:
			
				vetor = makeSet(n);
				v = 0;
				while(v != 3){
					printf("\nEscolha o que deseja fazer: \n");
					printf("1 - Criar união: \n");
					printf("2 - Mostrar Conjunto Disjunto: \n");
					printf("3 - Voltar: \n");
					scanf("%i",&v);
					switch(v){
						case 1:
							printf("Informe o primeiro valor da uniao: ");
							scanf("%i",&a);
							printf("Informe o segundo valor da uniao: ");
							scanf("%i",&b);

							vetor = unir(a,b,vetor);
						break;
						case 2:
							for(i = 0; i < n; i++){
								printf("%i",vetor[i]);
							}
						break;
						default:
							if(v != 3){
								printf("Opcao invalida \n");
								v = 0;
							}
						break;
					}
				}
			break;
			
			case 5:
				w = 0;
				while(w != 5){
					printf("1 - Construir Heap \n");
					printf("2 - Mostrar a heap \n");
					printf("3 - Retirar valor da heap \n");
					printf("4 - Pegar o valor da raiz \n");
					printf("5 - Sair \n");
					scanf("%i",&w);
					switch(w){
						case 1:
							no = g->ListaNodos;
							while(no != NULL){
								aux1 = no->adj;
								while(aux1 != NULL){
									tamanho++;
									heap = buildHeap(heap,tamanho,aux1);
									aux1 = aux1->prox;
								}
								no=no->prox;
    						}
    						printf("Heap construida com sucesso. \n");
						break;
						
						case 2:
							if(tamanho == 0){
								printf("Heap Vazia !!! \n");
							}
							else{
								printHeap(heap,tamanho);
								printf("\n");
							}
						break;
						
						case 3:
							if(tamanho != 0){
								removido = deleteHeap(heap,tamanho);
								printf("Removido:\n Partida: %i Destino:%i Peso: %i \n",removido->part, removido->chave_adj, removido->peso);
								tamanho--;
							}
							else{
								printf("Heap vazia !! \n");
							}
						break;
				
						case 4:
							if(tamanho != 0){
								raiz = getRaiz(heap);
							}
							else{
								printf("Heap vazia !! \n");
							}
						break;
				
						default:
							if(w != 5){
								printf("Opcao invalida \n");
								w = 0;
							}
						break;
					}
				}
			break;
			
			case 6:
				Kruskal(g);
			break;

			case 7:
				Prim(g);
			break;

			case 8:
				Dijkstra(g);
			break;
			
			case 9:
				imprime(g);
			break;

			default:
				if(e != 0){
					printf("Opcao invalida \n");
					e = 1;
				}			
			break;
		}
	}
	return 0;
}
