#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"

Pilha *CriaPilha(){
	Pilha *pl = (Pilha*)malloc(sizeof(Pilha));
	pl->topo = NULL;
	return pl;
} 

void pilhaPush(Pilha *pl, int num){
	Stack *a = (Stack*)malloc(sizeof(Stack));
	a->chave_pilha = num;
	if (!pl->topo) {         
        pl->topo = a;
        pl->topo->prox = NULL;
	}
	else{
		a->prox = pl->topo;
		pl->topo = a;
	}
}

int pop(Pilha *pl){
	int valor;
	Stack *aux = pl->topo;
	pl->topo = pl->topo->prox;
	valor = aux->chave_pilha;
	free(aux);
	return valor;
}

void libera(Pilha *pl){

	if(pl->topo != NULL){
		Stack* aux;
		while(aux != NULL){
			aux = pl->topo->prox;
			free(pl->topo);
			pl->topo = aux;
		}
	}
}