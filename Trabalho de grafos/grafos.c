#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grafos.h"
#include "pilha.h"
#include "fila.h"
#include "conjunto.h"
#include "heap.h"

//fução para escolher o modo de abertura do arquivo
FILE *abrearquivo(char modo,FILE *arquivo){
	switch(modo){
		case 'g':		
			if((arquivo=fopen("dados.txt","a"))==NULL){
				printf("erro na abertura \n");
			}
		break;
		case 'r':		
			if((arquivo=fopen("dados.txt","rt"))==NULL){
				printf("erro na abertura \n");
			}
		break;
	}
	if(arquivo==NULL){
			printf("Permissão negada \n");
	}
	return arquivo;
}

//função que inicia o descritor vazio
Grafo *Inicia(){
	Grafo *novo = (Grafo*)malloc(sizeof(Grafo));
	novo->ListaNodos = NULL;
	novo->tamanho = 0;
	return novo;
}

//função que cria a lista principal do grafo
Nodo *criaNodo(Nodo *no, int num){
	Nodo *novo = (Nodo*)malloc(sizeof(Nodo));
	novo->chave = num;
	novo->adj = NULL;
	novo->prox = NULL;

	novo->prox = no;
	no = novo;

	return no;
}

//função que cria lsita de adjacencia decada nodo do grafo
Nodo *adj(Nodo *no, int part, int dest, int peso){

	Nodo *aux, *partida;
	int a = 0;
	for (aux = no; aux != NULL; aux = aux->prox){
		if(aux->chave == part){
			partida = aux; 
			a++;
		}
		if(a==0 && aux->prox==NULL){
			printf("Nenhum Nodo encontrado \n ");
			return NULL;
		}
	}
	Aresta *aresta = (Aresta*)malloc(sizeof(Aresta));
	aresta->chave_adj = dest;
	aresta->prox = NULL;
	aresta->peso = peso;
	aresta->part = part;

	if(partida->adj == NULL){
		
		partida->adj = aresta;
	}
	else{
		aresta->prox = partida->adj;
		partida->adj = aresta;
	}
	return no;
}

//função que imprime o grafo 
void imprime(Grafo *g){

	Nodo *no;
	Aresta *aux1;
	no = g->ListaNodos;
	while(no != NULL){
		aux1 = no->adj;
		while(aux1 != NULL){
			printf("Chave: %i, ADJ: %i, Peso: %i \n",no->chave, aux1->chave_adj,aux1->peso);
			aux1 = aux1->prox;
		}
		no=no->prox;
    }	
}

//função da busca em profundidade
void dfs(Grafo *g, int num){

	int visitado[g->tamanho];
	int x=0, i = 0, indicador = 0, a = 0; 
	Nodo *aux, *partida, *no;
	no = g->ListaNodos;
	Aresta *aux2;
	Pilha *pilha = CriaPilha();

	for(x = 0; x < g->tamanho; x++){
		visitado[x] = 0;
	}

	pilhaPush(pilha,num);
	while(pilha->topo != NULL){
		
		num = pop(pilha);
		indicador = 0;
		for(x = 0; x < g->tamanho; x++){
			if(visitado[x] == num){
				indicador = 1;
			}
		}
		if(indicador == 0){
			visitado[i] = num;
			i++;
		}
		aux=g->ListaNodos;
		while(aux->chave != num){
			aux=aux->prox;
		}
		aux2 = aux->adj;
		while(aux2 != NULL){
			indicador = 0;
			for(x = 0; x < g->tamanho; x++){
				if(visitado[x] == aux2->chave_adj){
					indicador++;
				}
			}
			if(indicador == 0){
				pilhaPush(pilha,aux2->chave_adj);	
			}
			aux2 = aux2->prox;
		}	
		x++;		
	}
	
	for(x = 0; x < g->tamanho; x++){
		printf("[%i] \n",visitado[x]);
	}
	libera(pilha);
}

//função da busca em largura
void bfs(Grafo *g, int num){

	int visitado[g->tamanho];
	int x=0, i = 0, indicador = 0, a = 0; 
	Nodo *aux, *partida, *no;
	no = g->ListaNodos;
	Aresta *aux2;
	filaLista *fila = criarFilaLista();

	for(x = 0; x < g->tamanho; x++){
		visitado[x] = 0;
	}

	insereFilaLista(fila,num);
	while(fila->ini != NULL){
		
		num = retiraFilaLista(fila);
		indicador = 0;
		for(x = 0; x < g->tamanho; x++){
				if(visitado[x] == num){
					indicador++;
				}
		}
		if(indicador == 0){
			visitado[i] = num;
			i++;
		}	
		aux=g->ListaNodos;
		while(aux->chave != num){
			aux=aux->prox;
		}
		aux2 = aux->adj;
		while(aux2 != NULL){
			indicador = 0;
			for(x = 0; x < g->tamanho; x++){
				if(visitado[x] == aux2->chave_adj){
					indicador++;
				}
			}
			if(indicador == 0){
				insereFilaLista(fila,aux2->chave_adj);
			}
			aux2 = aux2->prox;
		}
		x++;
	}
	
	for(x = 0; x < g->tamanho; x++){
		printf("[%i] \n",visitado[x]);
	}
}	

void Kruskal(Grafo *g){
	int tamanho = 0, *vetor, x = 0, tm = 0, marcador = 0, i;
	Aresta *solucao = (Aresta*)malloc(sizeof(Aresta)*100);
	Nodo *no = g->ListaNodos;
	Aresta *aux1, *removido, *heap;
	heap = NULL;
	
	//percorre todo o grafo e monta a heap
	while(no != NULL){
		aux1 = no->adj;
		while(aux1 != NULL){
			tamanho++;
			heap = buildHeap(heap,tamanho,aux1);
			aux1 = aux1->prox;
		}
		no=no->prox;
    }	

    vetor = makeSet(g->tamanho);
    tm = tamanho;
    
    //enquanto a heap não estiver vazia repete o laço 
    for(tm = tamanho; tm > 0; tm--){
    	if(tamanho > 0){
    		//remove elemento da heap
    		removido = deleteHeap(heap,tamanho);
    		tamanho--;
    		//se os elementos não estiverem no mesmo conjunto coloca na solução e coloca eles no mesmo conjunto 
			if(findSet(removido->part,vetor) != findSet(removido->chave_adj,vetor)){
    			solucao[x].chave_adj = removido->chave_adj;
    			solucao[x].part = removido->part;
    			solucao[x].prox = removido->prox;
    			solucao[x].peso = removido->peso;
    			unir(findSet(removido->part,vetor),findSet(removido->chave_adj,vetor),vetor);
    			x++;
    		}	
		}
    }
    printf("Solucao: \n");
    for(int j = 0; j < x; j++){
    	printf("Partida: %i \nDestino: %i \nPeso: %i \n\n",solucao[j].part,solucao[j].chave_adj,solucao[j].peso);
	}
}

void *Prim(Grafo *g){
	int *vetor, num = 0, tamanho, x = 0, a,b;
	Nodo *no = g->ListaNodos;
	Aresta *aux, *heap = NULL, *removido;
	Aresta *solucao = (Aresta*)malloc(sizeof(Aresta)*100);
	
	vetor = makeSet(g->tamanho);
	//recebe do usuario o valor para começar o prim, coloca ele na solução
	printf("Informe por qual nodo deseja começar: ");
	scanf("%i",&num);
	solucao[x].chave_adj = num;
    solucao[x].part = num;
    solucao[x].prox = NULL;
    solucao[x].peso = 0;
	x++;
	//busca o valor na lista de grafos e coloca seus adjacente na heap
	while(no != NULL){
		if(no->chave == num){
			aux = no->adj;
			while(aux != NULL){
				tamanho++;
				heap = buildHeap(heap,tamanho,aux);
				aux = aux->prox;
			}
		}
		no = no->prox;
	}
	// enquanto tiver valor na heap repete o laço
	while(tamanho > 0){
		//remove elemento da heap
		removido = deleteHeap(heap,tamanho);
		tamanho--;
		//se os elementos não estiverem no mesmo conjunto coloca executa o resto do codigo
		if(findSet(removido->part,vetor) != findSet(removido->chave_adj,vetor)){
			//procura o elemento na lista do grafo e adiciona suas arestas na heap
			no = g->ListaNodos;
			while(no != NULL){
				if(no->chave == removido->chave_adj){
					aux = no->adj;
					while(aux != NULL){
						tamanho++;
						buildHeap(heap,tamanho,aux);
						aux = aux->prox;
					}
				}
				no = no->prox;
			}
			//colocar os valores no mesmo conjunto
			unir(findSet(removido->part,vetor),findSet(removido->chave_adj,vetor),vetor);
			solucao[x].chave_adj = removido->chave_adj;
    		solucao[x].part = removido->part;
    		solucao[x].prox = removido->prox;
    		solucao[x].peso = removido->peso;
    		x++;
		}
	}
 	for(int j = 0; j < x; j++){
    	printf("Partida: %i \nDestino: %i \nPeso: %i \n\n",solucao[j].part,solucao[j].chave_adj,solucao[j].peso);
	}
}

void *Dijkstra(Grafo *g){
	int d[g->tamanho],pi[g->tamanho], num = 0, tamanho = 0, cont = 0,marcador = 0;
	Aresta *solucao = (Aresta*)malloc(sizeof(Aresta)*100);
	Nodo *no;
	Aresta *aux, *removido, *heap = NULL;
	
	for(int x = 0; x < g->tamanho; x++){
		d[x] = 1000000;
		pi[x] = 0;
	}
	
	printf("Informe o nodo de partida: ");
	scanf("%i",&num);
	
	d[num - 1] = 0;
	solucao[cont].chave_adj= num;
	solucao[cont].part = num;
	solucao[cont].peso = 0; 
	cont++;
	no = g->ListaNodos;
	while(no != NULL){
		if(no->chave == num){
			aux = no->adj;
			while(aux != NULL){
				tamanho++;
				heap = buildHeap(heap,tamanho,aux);
				aux = aux->prox;
			}
		}
		no = no->prox;
	}
	while(tamanho > 0){
		removido = deleteHeap(heap,tamanho);
		tamanho--;
		marcador = 0;
		for(int x = 0; x < 100; x++){
			if(removido->chave_adj == solucao[x].chave_adj){
				 marcador++;
			}
		}	
		if(marcador == 0){
			solucao[cont].chave_adj= removido->chave_adj;
			solucao[cont].part = removido->part;
			solucao[cont].prox  = removido->prox;
			solucao[cont].peso = removido->peso;
			cont++;
		}
		marcador = 0;
	
		if(d[removido->chave_adj - 1] > (d[removido->part - 1] + removido->peso) ){
			pi[removido->chave_adj  - 1] = removido->part;
			d[removido->chave_adj - 1] = d[removido->part - 1] + removido->peso;
			no = g->ListaNodos;
			while(no != NULL){
				if(no->chave == removido->chave_adj){
					aux = no->adj;
					while(aux != NULL){
						tamanho++;
						heap = buildHeap(heap,tamanho,aux);
						aux = aux->prox;
					}
				}
				no = no->prox;
			}
		}
	}
	for(int j = 0; j < cont; j++){
    	printf("Partida: %i \nDestino: %i \nPeso: %i \n\n",num,solucao[j].chave_adj,d[solucao[j].chave_adj - 1]);
	}
}