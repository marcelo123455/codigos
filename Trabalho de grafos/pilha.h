

struct pilha{

	int chave_pilha;
	struct pilha *prox;
};
typedef struct pilha Stack;

struct descritorPilha{
	Stack *topo;
};
typedef struct descritorPilha Pilha;

Pilha *CriaPilha();
void pilhaPush(Pilha *pl, int num);
int pop(Pilha *pl);
void libera(Pilha *pl);