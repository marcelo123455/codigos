#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "grafos.h"
#include "pilha.h"
#include "fila.h"
#include "conjunto.h"
#include "heap.h"

Aresta *buildHeap(Aresta *heap,int tamanho,Aresta *elemento){
	Aresta *vetor; 
	int x, j;
	vetor = (Aresta*)malloc(sizeof(Aresta) * tamanho);

	if(heap != NULL){
		for(x = 0; x < tamanho; x++){
			vetor = heap;
		}
	}
	vetor[tamanho - 1].chave_adj = elemento->chave_adj;
	vetor[tamanho - 1].part = elemento->part;
	vetor[tamanho - 1].peso = elemento->peso;
	vetor[tamanho - 1].prox = elemento->prox;

	heap = vetor;
	
	percolate(heap,tamanho);
	return heap;
}

void percolate(Aresta *heap,int tamanho){
	
	Aresta *aux = (Aresta*)malloc(sizeof(Aresta));
	int i;
	i = tamanho - 1;
	for(int x = 0; x < tamanho; x++){
		if(heap[i].peso < heap[i - 1].peso && i > 0){
			aux->chave_adj = heap[i - 1].chave_adj;
			aux->part = heap[i - 1].part;
			aux->peso = heap[i - 1].peso;
			aux->prox = heap[i - 1].prox;
			heap[i - 1] = heap[i];
			heap[i].chave_adj = aux->chave_adj;
			heap[i].part = aux->part;
			heap[i].peso = aux->peso;
			heap[i].prox = aux->prox;
		}
		i--;
	}
}

void printHeap(Aresta *heap,int tamanho){
	int x;
	for(x = 0; x < tamanho; x++){
		printf("Partida : %i \n Destino: %i \n Peso: %i \n",heap[x].part, heap[x].chave_adj, heap[x].peso);
	} 
}

Aresta *deleteHeap(Aresta *heap,int tamanho){
	Aresta *aux = (Aresta*)malloc(sizeof(Aresta));
	Aresta *vetaux;
	aux->chave_adj = heap[0].chave_adj;
	aux->part = heap[0].part;
	aux->peso = heap[0].peso;
	aux->prox = heap[0].prox;;
	heap[0] = heap[tamanho - 1];
	vetaux = heap;
	heap=(Aresta*)malloc(sizeof(Aresta) * (tamanho - 1));
	heap = vetaux;
	siftDown(heap,tamanho - 1);
	return aux;
}

void siftDown(Aresta *heap,int tamanho){

	int i; 
	Aresta *aux = (Aresta*)malloc(sizeof(Aresta));
	for(int x = 0; x < tamanho; x++){
		if(heap[x].peso > heap[x + 1].peso){
			aux->chave_adj = heap[x + 1].chave_adj;
			aux->part = heap[x + 1].part;
			aux->peso = heap[x + 1].peso;
			aux->prox = heap[x + 1].prox;
			heap[x + 1] = heap[x];
			heap[x].chave_adj = aux->chave_adj;
			heap[x].part = aux->part;
			heap[x].peso = aux->peso;
			heap[x].prox = aux->prox;
		}
	} 
}

Aresta *getRaiz(Aresta *heap){
	Aresta *aux =(Aresta*)malloc(sizeof(Aresta));
	aux->chave_adj = heap[0].chave_adj;
	aux->part = heap[0].part;
	aux->peso = heap[0].peso;
	aux->prox = heap[0].prox;
	printf(" Raiz: \n Partida: %i \n Destino: %i \n Peso: %i \n",aux->part, aux->chave_adj, aux->peso);
	return aux;
}