

struct grafo{
	
	struct nodo *ListaNodos;
	int tamanho;
};
typedef struct grafo Grafo; 

struct nodo{
	
	int chave;
	struct nodo *prox;
	struct aresta *adj;
};
typedef struct nodo Nodo;

struct aresta{
	
	int chave_adj;
	struct aresta *prox;
	int peso;
	int part;
};
typedef struct aresta Aresta;

FILE *abrearquivo(char modo,FILE *arquivo);
Grafo *Inicia();
Nodo *criaNodo(Nodo *no, int num);
Nodo *adj(Nodo *no, int part, int dest, int peso);
Nodo *buscaNodo(Nodo *no,int num);
void dfs(Grafo *g, int num);
void bfs(Grafo *g, int num);
void Kruskal(Grafo *g);
void imprime(Grafo *g);
void *Prim(Grafo *g);
void *Dijkstra(Grafo *g);