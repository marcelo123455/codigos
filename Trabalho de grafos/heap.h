
//Biblioteca Heap

//função que constroi um novo heap com o elemento inserido
Aresta *buildHeap(Aresta *heap,int tamanho,Aresta *elemento);

//função para deletar o elemento da heap
Aresta *deleteHeap(Aresta *heap,int tamanho);

//função para imprimiro estado atual da heap
void printHeap(Aresta *heap,int tamanho);

//função sift-down para posicionar os elementos da heap
void siftDown(Aresta *heap,int tamanho);

//função percolate para posicionar os elementos da heap
void percolate(Aresta *heap,int tamanho);

//retorna o menor da heap 
Aresta *getRaiz(Aresta *heap);