#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fila.h"


//Fila Lista
filaLista *criarFilaLista(){
	filaLista *fl = (filaLista*)malloc(sizeof(filaLista));
	fl->ini = fl->fim = NULL;
	return fl; 
}

void insereFilaLista(filaLista *fl, int a) {
	Lista *novo = (Lista*)malloc(sizeof(Lista));
	novo->info = a;
	novo->prox = NULL;
	if(fl->fim == NULL && fl->ini == NULL){
		fl->ini = novo;
		fl->fim = novo;
	}
	else {
		fl->fim->prox = novo;
		fl->fim = novo;
	}
}

int retiraFilaLista(filaLista *fl){
	Lista *aux;
	int a;
	if(fl->fim == NULL && fl->ini == NULL){
		printf("Lista Vazia!!! \n");
		exit(1);
	}
	else{
		aux = fl->ini;
		a = aux->info;
		fl->ini = aux->prox;
		if(fl->ini == NULL){
			fl->fim = NULL;
		}
		free(aux);	
		return a;
	}
}

void liberaFilaLista(filaLista *fl){
	Lista *q = fl->ini;
	 while (q != NULL){
	 	Lista *t = q->prox;
	 	free(q);
	 	q = t;
	 }
	 free(fl);
}
